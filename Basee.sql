
CREATE database final;
use final;

CREATE TABLE utilisateur (
    idadmin INT AUTO_INCREMENT PRIMARY KEY,
    mail VARCHAR(20),
    mdp CHAR(8), 
    role ENUM('Admin','Utilisateur')
);

CREATE TABLE variete_the (
    idvariete_the INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(20),
    occupation FLOAT,
    rendement FLOAT
);

CREATE TABLE parcelle (
    idparcelle INT AUTO_INCREMENT PRIMARY KEY,
    numero INT,
    surface FLOAT,
    idvariete_the INT,
    FOREIGN KEY (idvariete_the) REFERENCES variete_the(idvariete_the)
);

CREATE TABLE cueilleur (
    idcueilleur INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(20),
    adresse VARCHAR(20)
);

CREATE TABLE categorie_depense (
    idcategorie_depense INT AUTO_INCREMENT PRIMARY KEY,
    categorie TEXT
);

CREATE TABLE salaire (
    idsalaire INT AUTO_INCREMENT PRIMARY KEY,
    kgmin FLOAT NOT NULL,
    kgmax FLOAT NOT NULL,
    montant DECIMAL(10,2)
);

CREATE TABLE cueillette (
    idcueillette INT AUTO_INCREMENT PRIMARY KEY,
    idcueilleur INT,
    idparcelle INT,
    poids FLOAT,
    date DATE,
    FOREIGN KEY (idcueilleur) REFERENCES cueilleur(idcueilleur),
    FOREIGN KEY (idparcelle) REFERENCES parcelle(idparcelle)
);

CREATE TABLE depense (
    iddepense INT AUTO_INCREMENT PRIMARY KEY,
    idcategorie_depense INT,
    montant DECIMAL(10,2),
    date DATE,
    FOREIGN KEY (idcategorie_depense) REFERENCES categorie_depense(idcategorie_depense)
);

-- Vues

CREATE OR REPLACE VIEW v_poids_total AS
SELECT ROUND(COALESCE(SUM(poids), 0.00), 2) AS poids_total
FROM cueillette;

CREATE OR REPLACE VIEW v_poids_restant AS
SELECT 
    p.idparcelle,
    EXTRACT(MONTH FROM gs.mois) AS mois_recolte,
    EXTRACT(YEAR FROM gs.mois) AS annee_recolte,
    ROUND(COALESCE((p.surface * 10000 * vt.rendement) - COALESCE(SUM(c.poids), 0), 0), 2) AS poids_restant
FROM 
    parcelle p
CROSS JOIN (
    SELECT DISTINCT DATE_FORMAT(c.date, '%Y-%m-01') AS mois
    FROM cueillette c
) gs
LEFT JOIN 
    cueillette c ON p.idparcelle = c.idparcelle AND DATE_FORMAT(c.date, '%Y-%m-01') = gs.mois
JOIN 
    variete_the vt ON p.idvariete_the = vt.idvariete_the
GROUP BY 
    p.idparcelle, mois_recolte, annee_recolte, p.surface, vt.rendement
ORDER BY 
    annee_recolte, mois_recolte, p.idparcelle;

CREATE OR REPLACE VIEW v_cout_revient AS 
SELECT 
    COALESCE(SUM(d.montant), 0) / COALESCE(SUM(c.poids),1) AS cout_revient_par_kg
FROM 
    cueillette c
LEFT JOIN 
    depense d ON c.date = d.date
GROUP BY 
    c.date;

CREATE OR REPLACE VIEW v_variete_parcelle AS
SELECT
    p.idparcelle,
    p.numero,
    p.surface,
    v.idvariete_the,
    v.nom AS nom_variete,
    v.occupation,
    v.rendement
FROM
    parcelle p
JOIN
    variete_the v ON p.idvariete_the = v.idvariete_the;

CREATE OR REPLACE VIEW v_cueillette AS 
SELECT c.idcueillette, c.poids, c.date, cue.nom AS nom_cueilleur, p.numero AS numero_parcelle
FROM cueillette c
JOIN cueilleur cue ON c.idcueilleur = cue.idcueilleur
JOIN parcelle p ON c.idparcelle = p.idparcelle;

CREATE OR REPLACE VIEW v_depense_categorie AS 
SELECT d.* , c.categorie FROM depense d JOIN categorie_depense c USING(idcategorie_depense);
